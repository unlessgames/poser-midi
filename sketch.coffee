class v2
  constructor : (@x = 0, @y = 0) ->
  set : (_x, _y) ->
    @x = _x
    @y = _y

CHANNEL = 1
WIDTH = 640
HEIGHT = 480

midi_out = null

maps = 
  leftWristX : 0
  leftWristY : 1
  rightWristX : 2
  rightWristY : 3
  shouldersWidth : 4
  shouldersY : 5
  leftAnkleX : 6
  leftAnkleY : 7
  rightAnkleX : 8
  rightAnkleY : 9
  leftKneeX : 10
  leftKneeY : 11
  rightKneeX : 12
  rightKneeY : 13
  leftElbowX : 10
  leftElbowY : 11
  rightElbowX : 12
  rightElbowY : 13
  hips : 14
  headX : 15
  headY : 16
  headR : 17

values = Object.keys(maps).map((k, i) -> 0)

send_cc = (c, v) ->
  values[c] = v
  midi_out.sendControlChange(c, v, CHANNEL)

clamp = (v, a, b) ->
  if v < a then a else if v > b then b else v

cmap = (x, a, b, c, d) ->
  Math.floor(clamp(map(x, a, b, c, d), c, d))

remap = 
  x : (_x) -> cmap(_x, 0, WIDTH, 0, 127)
  y : (_y) -> cmap(_y, 0, HEIGHT, 0, 127)

parts = 
  leftWrist : (x, y) ->
    send_cc maps.leftWristX, remap.x(x)
    send_cc maps.leftWristY, remap.y(y)
  rightWrist : (x, y) ->
    send_cc maps.rightWristX, remap.x(x)
    send_cc maps.rightWristY, remap.y(y)

  shoulders : 
    left : new v2()
    right : new v2()

  leftShoulder : (x, y) ->
    @shoulders.left.set x, y
    v = Math.abs(x - @shoulders.right.x)
    send_cc maps.shouldersWidth, cmap(v, 0, 200, 0, 127)
    send_cc maps.shouldersY, remap.y(((y + @shoulders.right.y) / 2))
  rightShoulder : (x, y) ->
    @shoulders.right.set x, y
    v = Math.abs(x - @shoulders.left.x)
    send_cc maps.shouldersWidth, cmap(v, 0, 200, 0, 127)
    send_cc maps.shouldersY, remap.y(((y + @shoulders.left.y) / 2))
  leftAnkle : (x, y) ->
    send_cc maps.leftAnkleX, cmap(x, 0, WIDTH, 0, 127)
    send_cc maps.leftAnkleY, cmap(y, 0, HEIGHT / 1.5, 0, 127)
  rightAnkle : (x, y) ->
    send_cc maps.rightAnkleX, cmap(x, 0, WIDTH, 0, 127)
    send_cc maps.rightAnkleY, cmap(y, 0, HEIGHT / 1.5, 0, 127)

  leftKnee : (x, y) ->
    send_cc maps.leftKneeX, cmap(x, 0, WIDTH, 0, 127)
    send_cc maps.leftKneeY, cmap(y, 0, HEIGHT / 1.5, 0, 127)
  rightKnee : (x, y) ->
    send_cc maps.rightKneeX, cmap(x, 0, WIDTH, 0, 127)
    send_cc maps.rightKneeY, cmap(y, 0, HEIGHT / 1.5, 0, 127)

  leftElbow : (x, y) ->
    send_cc maps.leftElbowX, remap.x(x)
    send_cc maps.leftElbowY, remap.y(y)
  rightElbow : (x, y) ->
    send_cc maps.rightElbowX, remap.x(x)
    send_cc maps.rightElbowY, remap.y(y)

  hips : 
    left : new v2()
    right : new v2()
  leftHip : (x, y) ->
    @hips.left.set x, y
    v = Math.abs(x - @hips.right.x)
    send_cc(maps.hips, cmap(v, 0, WIDTH / 5, 0, 127))
  rightHip : (x, y) ->
    @hips.right.set x, y
    v = Math.abs(x - @hips.left.x)
    send_cc(maps.hips, cmap(v, 0, WIDTH / 5, 0, 127))

  head : 
    nose : new v2()
    leftEye : new v2()
    rightEye : new v2()
    posX : () -> (@nose.x + @leftEye.x + @rightEye.x) / 3
    posY : () -> (@nose.y + @leftEye.y + @rightEye.y) / 3
    rot : () -> @leftEye.y - @rightEye.y
    output : () ->
      send_cc maps.headX, cmap(@posX(), 0, WIDTH, 0, 127)
      send_cc maps.headY, cmap(@posY(), 0, HEIGHT, 0, 127)
      send_cc maps.headR, cmap(@rot(), -100, 100, 0, 127)
  leftEye : (x, y) ->
    @head.leftEye.set x, y
    @head.output()
  nose : (x, y) ->
    @head.nose.set x, y
    @head.output()
  rightEye : (x, y) ->
    @head.rightEye.set x, y
    @head.output()


video = null
poseNet = null
poses = []

WebMidi.enable((err) ->
  midi_out = WebMidi.outputs[0]
)

setup = () ->
  createCanvas 640, 480
  video = createCapture VIDEO
  video.size width, height

  poseNet = ml5.poseNet video, modelReady
  # poseNet = ml5.poseNet(video, {flipHorizontal : true}, modelReady)
  poseNet.on 'pose', (results) -> poses = results
  video.hide()

modelReady = () ->
  select('#status').html('Model Loaded')

drawValues = () ->
  h = HEIGHT / values.length
  fill "#00000066"
  rect 0, 0, 190, height
  fill(255)
  noStroke()
  textSize(18)
  Object.keys(maps).map((k, i) -> text("#{k} : #{values[i]}", 10, h * i))

drawPose = () ->
  radius = 15
  for _pose, i in poses
    if i == 0
      pose = _pose.pose

      skeleton = _pose.skeleton
      for part in skeleton
        partA = part[0]
        partB = part[1]
        stroke(255, 0, 0)
        line(partA.position.x, partA.position.y, partB.position.x, partB.position.y)

      for p in pose.keypoints
        if p.score > 0.2 && midi_out?
          if parts[p.part]?
            parts[p.part](p.position.x, p.position.y)
            fill(lerpColor(color(255,0,0), color(0,255,0), p.score - 0.2))
            noStroke()
            ellipse(p.position.x, p.position.y, radius, radius)

draw = () ->
  image(video, 0, 0, width, height)
  drawPose()
  drawValues() 

